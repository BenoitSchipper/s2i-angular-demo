# Angular/NGINX S2I builder Image & Deployment Example

- In this example we first use S2I to prepare a base angular-nginx image using the base nginx:latest image.
- We then show an example on howe to build your application image using the angular-nginx base image and deploy the application.

## Usage

- Initially use the "baseImage-buildConfig.yaml" to create your base angular-nginx image.
- This angular-nginx Image will be used for all your front-end applications.

- Then build your application with "angularApp-buildConfig.yaml" using the above base image you have just created.
- Then deploy your front-end application using the "angularApp-Deployment.yaml".

## More Info

- In this example we use a demo front-end found here: https://github.com/gothinkster/angular-realworld-example-app.git
