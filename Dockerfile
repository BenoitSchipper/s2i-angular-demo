FROM scratch

USER root

COPY ./s2i/bin/assemble /usr/libexec/s2i

RUN yum -y install npm           && \
    npm install -g ng-cli        && \
    yum clean all -y

USER 1001
